from django.db import models

class PostalCodes(models.Model):
    postalcode = models.CharField( db_column='postal_code',
                                   primary_key=True,
                                   max_length=6)  # Field name made lowercase.-

    fsa = models.CharField(db_column='fsa',
                           max_length=3,
                           blank=True,
                           null=True)  # Field name made lowercase.

    latitude = models.DecimalField(db_column='latitude',
                                   max_digits=11,
                                   decimal_places=8,
                                   blank=True,
                                   null=True)  # Field name made lowercase.

    longitude = models.DecimalField(db_column='longitude',
                                    max_digits=11,
                                    decimal_places=8,
                                    blank=True,
                                    null=True)  # Field name made lowercase.

    placename = models.CharField(db_column='place_name',
                                 max_length=50,
                                 blank=True,
                                 null=True)  # Field name made lowercase.

    fsa1 = models.CharField(db_column='fsa1',
                            max_length=3,
                            blank=True,
                            null=True)  # Field name made lowercase.

    fsaprovince = models.IntegerField(db_column='fsa_province',
                                      blank=True,
                                      null=True)  # Field name made lowercase.

    areatype = models.CharField(db_column='area_type',
                                max_length=10,
                                blank=True,
                                null=True)  # Field name made lowercase.

    class Meta:
      #  managed = True
        db_table = 'postal_codes'
 #       app_label = 'mygis'

        def __str__(self):
            return self.postalcode