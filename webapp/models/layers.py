from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Layers(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(db_column='name',
                            max_length=30,
                            blank=False,
                            null=False)

    geoserver_layername = models.CharField(db_column='geoserver_layername',
                                       max_length=30,
                                       blank=False,
                                       null=False)

    geoserver_workspace = models.CharField(db_column='geoserver_workspace',
                                           max_length=30,
                                           blank=True,
                                           null=True)

    transparency = models.DecimalField(db_column='transparency',
                                       decimal_places=2,
                                       max_digits=3,
                                       validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
                                       blank=False,
                                       null=False)  # FixMe: validator for alpha channel

    min_zoom_scale = models.PositiveIntegerField(db_column='min_zoom_scale',
                                                 validators=[MinValueValidator(0), MaxValueValidator(21)],
                                                 blank=False,
                                                 null=False)

    max_zoom_scale = models.PositiveIntegerField(db_column='max_zoom_scale',
                                                 validators=[MinValueValidator(0), MaxValueValidator(21)],
                                                 blank=False,
                                                 null=False)

    class Meta:
      #  managed = True
        db_table = 'layers'
        #app_label = 'mygis'
        verbose_name_plural='Layers'


    def __str__(self):
        return self.name
