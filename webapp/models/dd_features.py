from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Features(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(db_column='name',
                            max_length=30,
                            blank=False,
                            null=False)

    geom_type = models.CharField(db_column='geom_type',
                                       max_length=30,
                                       blank=False,
                                       null=False)

    primary_geom_name = models.CharField(db_column='primary_geom_name',
                                           max_length=30,
                                           blank=False,
                                           null=False)

    class Meta:
      #  managed = True
        db_table = 'features'
        verbose_name_plural = 'Features'


        def __str__(self):
            return self.name