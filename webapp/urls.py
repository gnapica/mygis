from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [

    path('', views.HomePageView.as_view(), name='home'),
    path('pc/', views.PostalCodesView.as_view(), name='create'),
    path('about/', views.AboutPageView.as_view(), name='about'),
]

urlpatterns += staticfiles_urlpatterns()
