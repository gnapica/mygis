from django.views.generic import TemplateView
from functools import reduce
import operator
from rest_framework import generics
from .serializer import PostalCodesSerializer
from .models import PostalCodes
import operator
from functools import reduce

from django.views.generic import TemplateView
from rest_framework import generics

from .models import PostalCodes
from .serializer import PostalCodesSerializer


# Create your views here.

class HomePageView(TemplateView):
    template_name = 'home.html'


class AboutPageView(TemplateView):
    template_name = 'about.html'


from django.db.models import Q

from django.core.paginator import Paginator


# Create your views here.


class SearchPageView(TemplateView):
    template_name = 'search.html'


class PostalCodesView(generics.ListAPIView):
    """
    provides a get method handler
    """
    #    queryset = PostalCodes.objects.all()
    serializer_class = PostalCodesSerializer

    def get_queryset(self):
        ip = self.request.query_params.get('postalcode', None)
        return Paginator(PostalCodes.objects.filter(postalcode__startswith=ip).order_by('postalcode'), 10).get_page(1)


class PostalCodesListView(SearchPageView):
    """
    Display a Blog List page filtered by the search query.
    """
    paginate_by = 10

    def get_queryset(self):
        result = super(PostalCodesListView, self).get_queryset()

        query = self.request.GET.get('postalcode')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(title__icontains=postalcode) for postalcode in query_list)) |
                reduce(operator.and_,
                       (Q(content__icontains=postalcode) for postalcode in query_list))
            )

        return result


class AboutPageView(TemplateView):
    template_name = 'about.html'
