from django.contrib.auth.models import User, Group
from rest_framework import serializers

from webapp.models.postal_codes import PostalCodes
from webapp.models.layers import Layers
from webapp.models.dd_features import Features



class PostalCodesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostalCodes
        fields = ('postalcode','latitude','longitude')



class LayersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layers
        fields = ('name')


class FeaturesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Features
        fields = ('name')