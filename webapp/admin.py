from django.contrib import admin
from webapp.models.dd_features import Features
from webapp.models.layers import Layers
from django.contrib.auth.models import User, Group

admin.site.site_header = 'myGis Administration'
admin.site.site_title = 'Admin'
admin.site.site_url = 'http://gnapi.ca/'
admin.site.index_title = 'Welcome to myGis Administration'
admin.empty_value_display = '**Empty**'



#Update Records Display Label
class LayersAdmin(admin.ModelAdmin):
    list_display = ['name', 'min_zoom_scale','max_zoom_scale', 'geoserver_layername','geoserver_workspace']

class FeaturesAdmin(admin.ModelAdmin):
    list_display = ['name', 'geom_type', 'primary_geom_name']



# Register your models here.
admin.site.register(Layers, LayersAdmin)
admin.site.register(Features, FeaturesAdmin)


#Unregister Users and Groups
admin.site.unregister(User)
admin.site.unregister(Group)