CREATE TABLE public.dd_layers
(
  id SERIAL PRIMARY KEY ,
  name  VARCHAR (20) NOT NULL,
  external_name  VARCHAR (30) NOT NULL,
  geoserver VARCHAR (30) NOT NULL,
  geoserver_workspace VARCHAR (30) NOT NULL,
  geoserver_layer VARCHAR (30) NOT NULL,
  min_zoom integer DEFAULT 7,
  max_zoom integer DEFAULT 20,
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dd_layers
  OWNER TO postgres;