/*Link the layers visible in each application*/

CREATE TABLE public.app_layer
(
  app_id integer NOT NULL,
  layer_id integer Not Null,
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.app_layer
  OWNER TO postgres;