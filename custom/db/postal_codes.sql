-- Table: public.postal_codes

-- DROP TABLE public.postal_codes;

CREATE TABLE public.postal_codes
(
  postal_code character varying(6) NOT NULL,
  fsa character varying(3),
  latitude numeric(11,8),
  longitude numeric(11,8),
  place_name character varying(50),
  fsa1 character varying(3),
  fsa_province integer,
  area_type character varying(10),
  CONSTRAINT pk_postalcode PRIMARY KEY (postal_code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.postal_codes
  OWNER TO postgres;
