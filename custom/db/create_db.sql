-- Database: mygis

-- DROP DATABASE mygis;

CREATE DATABASE mygis
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'C.UTF-8'
       LC_CTYPE = 'C.UTF-8'
       CONNECTION LIMIT = -1;



CREATE EXTENSION POSTGIS;
CREATE EXTENSION HSTORE;
