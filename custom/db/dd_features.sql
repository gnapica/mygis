CREATE TABLE public.dd_features
(
  id SERIAL PRIMARY KEY ,
  name  VARCHAR (20) NOT NULL,
  geom_type VARCHAR (30) NOT NULL,
  owner VARCHAR (30) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.dd_features
  OWNER TO postgres;